;(function (angular) {
  'use strict';

  angular
    .module('peopleService', [])
    .service('peopleService', PeopleServiceConstructor)
  ;

  function PeopleServiceConstructor($http, $log, $q) {
    var svc = this;

    svc.getList = function () {

      var httpPromise = $http.get('http://swapi.co/api/people/');

      var unpackDataPromise = httpPromise.then(function (response) {
        return response.data.results;
      });

      return unpackDataPromise;
    };

    svc.getPerson = function (personId) {

      var person;

      function unpackPersonDataAndReturnHomeworld(personResponse) {
        person = personResponse.data;
        return $http.get(person.homeworld);
      }

      return $http.get('http://swapi.co/api/people/' + personId)
        .then(unpackPersonDataAndReturnHomeworld)
        .then(function (homeworldResponse) {
          person.homeworldData = homeworldResponse.data;

          var residentPromises = [];

          angular.forEach(homeworldResponse.data.residents, function (residentUrl) {
            $log.debug('One resident url:', residentUrl);
            residentPromises.push(
              $http.get( residentUrl )
                .then(function (response) { return response.data; })
            );
          });

          $log.debug('residentPromises:', residentPromises);

          return $q.all( residentPromises );
        })
        .then(function (residentResponse) {

          $log.debug('residentResponse:', residentResponse);
          
          person.residentData = residentResponse;

          return person;
        })
      ;

    };

  }


})(window.angular);

