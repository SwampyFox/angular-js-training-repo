;(function (angular) {
  'use strict';

  function ArithmeticController(
    addTwoNumbers,
    handyMathUtils,
    handyMathUtilsService
  ) {
    var ac = this;

    ac.sum = addTwoNumbers(5, 7);

    ac.mod = handyMathUtilsService.modulus(7, 5);

    ac.mult = handyMathUtilsService.multiply(7, 5);
  }
  
  function adder(first, second) {
    return first + second;
  }


  angular
    .module('mathExamples', ['ngRoute'])
    .component('mathExamples', {
      templateUrl: 'math.html',
      controller: ArithmeticController,
      controllerAs: 'ac'
    })

    .config(function ($routeProvider) {
      $routeProvider
        .when('/math', { template: '<math-examples></math-examples>' })
      ;
    })

    .factory('addTwoNumbers', function () {
      return adder;
    })

    .factory('handyMathUtils', function () {

      return {
        modulus: function (numerator, denominator) {
          return numerator % denominator;
        },

        multiply: function (op1, op2) {
          return op1 * op2;
        }
      };

    })

    .service('handyMathUtilsService', function () {
      var svc = this;

      svc.modulus = function  (numerator, denominator) {
        return numerator % denominator;
      };

      svc.multiply = function (op1, op2) {
        return op1 * op2;
      };
    })
  ;


})(window.angular);

