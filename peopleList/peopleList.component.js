;(function (angular) {
  'use strict';

  angular
    .module('peopleListComponent', [])
    .component('peopleList', {
      templateUrl: 'peopleList/template.html',
      controller: PeopleListController,
      controllerAs: 'ec',
      bindings: {
        caption: '@',
        people: '='
      }
    })
  ;

  function PeopleListController() {
    var ec = this;
    
    ec.name = 'Larry';

    ec.addPerson = function () {
      ec.people.push(
        {
          name: ec.name
        }
      );
    };

    ec.showName = function () {
      console.log('The name is ' + ec.name); 
    };
  }

})(window.angular);

