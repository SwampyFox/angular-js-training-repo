;(function (angular) {

  'use strict';

  angular.module('sandbox.constants', [])
    .constant('APP_TITLE', 'Angular Sandbox')
    .constant('APP_CONFIG', {
      BASE_API_URL: 'http://example.com/',
      FAVORITE_ANIMAL: 'giraffe'
    })
  ;    

})(window.angular);
