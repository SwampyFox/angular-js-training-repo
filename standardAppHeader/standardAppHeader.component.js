;(function (angular) {
  'use strict';

  angular
    .module('standardAppHeaderComponent')
    .component('standardAppHeader', {
      templateUrl: 'standardAppHeader/standard-app-header.html',
      transclude: true,
      bindings: {
        tagline: '@',
        logoImage: '@'
      },
      controller: function (APP_TITLE, APP_CONFIG, sharedObject) {
        var sah = this;

        sah.sobj = sharedObject;

        sah.appTitle = APP_TITLE;

        APP_CONFIG.FAVORITE_ANIMAL = 'lemur';

        sah.faveAnimal = APP_CONFIG.FAVORITE_ANIMAL;
      },
      controllerAs: 'sah'
    })
  ;

})(window.angular);

