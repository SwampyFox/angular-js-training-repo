;(function (angular) {
  'use strict';

  angular
    .module('sandbox', [
      'ngRoute',

      'mathExamples',

      'standardAppHeaderComponent',
      'peopleListComponent',
      'peopleService'
    ])

    .config(function ($routeProvider) {
    
      $routeProvider

        .when('/shared', {
          templateUrl: 'shared-object.html'
        })
        .when('/swperson', {
          templateUrl: 'swperson.html'
        })
        .when('/people', {
          templateUrl: 'people.html'
        })
        .otherwise({
          redirectTo: '/math'
        })
      ;
    
    })

    .controller('SandboxAppController', SandboxAppController)

    .constant('sharedObject', {
      widgetCount: 17
    })

/*
    .factory('five', function () {
      return 5;
    })
*/
    .constant('five', 5)

  ;




  function SandboxAppController(
      $q,
      $http,
      peopleService,
      $timeout,
      $log,
      five,
      sharedObject
  ) {
    var mc = this;

    mc.someStyleObject = {
      border: '3px solid green',
      borderRadius: '10px',
      padding: '10px',
      margin: '10px'
    };
  
    mc.obj = sharedObject;

    $log.debug('Five is... ', five);

//    mc.peopleList = [
//      { first_name: 'George', last_name: 'Washington'   },
//      { first_name: 'Thomas', last_name: 'Jefferson'    },
//      { first_name: 'John',   last_name: 'Adams'        },
//      { first_name: 'Grover', last_name: 'Cleveland'    }
//    ];

//    $http.get('../demo-data/workers.json')
//      .then(function (response) {
//        mc.peopleList = response.data;
//      })
//    ;
//
    peopleService.getList()
      .then(function (data) {
        mc.peopleList = data;
      })
    ;

    var p7 = peopleService.getPerson(7).then(function (personData) {
      mc.swPerson = personData;
      return mc.swPerson;
    });

    var p3 = peopleService.getPerson(3);

    var p6 = peopleService.getPerson(6);

    var pAll = $q.all({ Beru: p7, Droid: p3, Owen: p6 })
      .then(function (results) {
    
        $log.info('All done! Results:', results);

      })
      .catch(function (err) {
        $log.error('Got this error:', err);
      })
    ;


    $log.info('People list:', mc.peopleList);
    
    $timeout(function () {

      $log.info('Time is up!');

    }, 3000);
    
  }

})(window.angular);
